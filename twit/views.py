from django.shortcuts import render, redirect
from .models import Status
from .forms import PostForm

# Create your views here.
def index(request):
    form = PostForm()
    query_results = Status.objects.all()
    response = {'query_results': query_results, 'form': form}
    return render(request, 'index.html', response)

def post_status(request):
    form = PostForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response = {}
        response['text'] = request.POST['text']
        status = Status(text=response['text'])

        status.save()
        return redirect('/')
    else:
        return redirect('/')