from django import forms

from .models import Status

class PostForm(forms.ModelForm):

    class Meta:
        model = Status
        fields = ('text', )