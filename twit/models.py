from django.db import models

# Create your models here.
class Status(models.Model):
    text = models.TextField(max_length=300)
    time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(time)