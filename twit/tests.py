from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Status

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class LandingPageUnitTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_landingpage_url(self):
        page = self.client.get('')
        self.assertTrue(page.status_code, 200)
    
    def test_landingpage_template(self):
        page = self.client.get('')
        self.assertTemplateUsed(page, 'index.html')
    
    def test_landingpage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_landingpage_contains_hello(self):
        page_content = self.client.get('').content.decode('utf8')
        self.assertIn('Hello, Apa Kabar?', page_content)
    
    def test_model_can_create_new_status(self):
        status = Status.objects.create(text="COBA")
        self.assertEqual(Status.objects.all().count(), 1)

    def test_status_displayed_on_page(self):
        text = "COBA2"
        status = Status.objects.create(text=text)
        page_content = self.client.get('').content.decode('utf8')
        self.assertIn(text, page_content)
    
    def test_300char_limit(self):
        text = 'a' * 301
        self.client.post('', {'text': text})
        self.assertEqual(Status.objects.filter(text=text).count(), 0)

    

    '''
    def test_is_status_valid(self):
        #get status
        status = "a" * 301
        self.assertTrue(len(status) <= 300)
    '''        

''' 
class StatusTest(TestCase):
    def setUp(self):
        Status.objects.create(text="The quick brown fox jumps over the lazy dog")
'''

class Story7FunctionalTest(TestCase):
    
    def setUp(self):
        #chrome_options = Options()
        self.selenium = webdriver.Chrome('./chromedriver.exe')
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_input_form(self):
        self.selenium.get('http://127.0.0.1:8000/')
        self.selenium.implicitly_wait(10)

        textarea = self.selenium.find_element_by_tag_name('textarea')
        submit = self.selenium.find_element_by_tag_name('button')

        test_str = 'COBA COBA'

        textarea.send_keys(test_str)
        submit.send_keys(Keys.RETURN)

        self.assertIn(test_str, self.selenium.page_source)

        self.selenium.implicitly_wait(10)

    #[LAYOUT] Tes tabel status
    def test_is_table_loaded(self):
        self.selenium.get('http://127.0.0.1:8000')
        self.selenium.implicitly_wait(3)

        try:
            table = self.selenium.find_element_by_tag_name('table')
            self.assertTrue(True)
        except:
            self.assertTrue(False)

    #[LAYOUT] Tes judul
    def test_page_title(self):
        self.selenium.get('http://127.0.0.1:8000')

        title = self.selenium.find_element_by_name('title')
        self.assertEqual('Hello, Apa Kabar?', title.text)
        self.assertEqual('h3', title.tag_name)
        self.selenium.implicitly_wait(3)

    #[STYLING] Cek bootstrap
    def test_bootstrap(self):
        self.selenium.get('http://127.0.0.1:8000')
        
        head_link = self.selenium.find_element_by_name('bootstrap')
        self.assertEqual('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', head_link.get_attribute('href'))

    #[STYLING] Cek styling tabel status
    def test_table_class(self):
        self.selenium.get('http://127.0.0.1:8000')

        table = self.selenium.find_element_by_tag_name('table')
        self.assertEqual('table', table.get_attribute('class'))